\contentsline {chapter}{\numberline {1}WST\IeC {\k E}P}{3}
\contentsline {section}{\numberline {1.1}Aplikacje internetowe}{3}
\contentsline {section}{\numberline {1.2}Cel i zakres pracy}{4}
\contentsline {section}{\numberline {1.3}Uzasadnienie wyboru tematu}{5}
\contentsline {chapter}{\numberline {2}ZA\IeC {\L }O\IeC {\.Z}ENIA PROJEKTOWE}{6}
\contentsline {section}{\numberline {2.1}Cele aplikacji}{6}
\contentsline {section}{\numberline {2.2}Profil u\IeC {\.z}ytkownika}{6}
\contentsline {section}{\numberline {2.3}Wymagania projektu}{6}
\contentsline {subsection}{\numberline {2.3.1}Wymagania funkcjonalne}{6}
\contentsline {subsection}{\numberline {2.3.2}Wymagania niefunkcjonalne}{7}
\contentsline {chapter}{\numberline {3}DOKUMENTACJA PROJEKTOWA}{8}
\contentsline {section}{\numberline {3.1}Technologie wykorzystane w pracy dyplomowej}{8}
\contentsline {subsection}{\numberline {3.1.1}Dzia\IeC {\l }aj\IeC {\k a}ce po stronie klienta}{8}
\contentsline {subsubsection}{Javascript, JQuery, JQuery UI}{8}
\contentsline {subsubsection}{HTML5}{9}
\contentsline {subsubsection}{Google Maps API, jQuery UI Map}{10}
\contentsline {subsubsection}{Ajax}{11}
\contentsline {subsubsection}{CSS3}{13}
\contentsline {subsubsection}{Bootstrap}{14}
\contentsline {subsection}{\numberline {3.1.2}Dzia\IeC {\l }aj\IeC {\k a}ce po stronie serwera}{14}
\contentsline {subsubsection}{PHP}{14}
\contentsline {subsubsection}{MySQL}{15}
\contentsline {section}{\numberline {3.2}Narz\IeC {\k e}dzia programistyczne}{16}
\contentsline {chapter}{\numberline {4}IMPLEMENTACJA}{17}
\contentsline {section}{\numberline {4.1}Architektura systemu}{17}
\contentsline {section}{\numberline {4.2}Diagram przypadk\IeC {\'o}w u\IeC {\.z}ycia}{18}
\contentsline {section}{\numberline {4.3}Schemat bazy danych}{18}
\contentsline {section}{\numberline {4.4}Fragmenty kodu}{19}
\contentsline {chapter}{\numberline {5}TESTY}{23}
\contentsline {chapter}{\numberline {6}DOKUMENTACJA U\IeC {\.Z}YTKOWA}{28}
\contentsline {section}{\numberline {6.1}Wymagania sprz\IeC {\k e}towe}{28}
\contentsline {section}{\numberline {6.2}Instrukcja korzystania z serwisu}{28}
\contentsline {chapter}{\numberline {7}PODSUMOWANIE}{32}
\contentsline {chapter}{\numberline {8}LITERATURA}{33}
