<?php
/**
 * Created by PhpStorm.
 * User: Ada
 * Date: 2015-03-18
 * Time: 19:45
 */

include 'config.php';
connect_to_db();

$place_id = $_GET['id'];
$upload_dir = 'uploads/';
$allowed_ext = array('jpg','jpeg','png','gif');

if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
    exit_status('Błąd! Niepoprawna metoda HTTP!');
}

if(array_key_exists('pic',$_FILES) && $_FILES['pic']['error'] == 0 ){

    $pic = $_FILES['pic'];

    if(!in_array(get_extension($pic['name']),$allowed_ext)){
        exit_status('Tylko pliki '.implode(',',$allowed_ext).' są dozwolone!');
    }

    // przeniesienie obrazu do tymczasowgo katalogu

    $new_name = $place_id.'-'.time().'.'.get_extension($pic['name']);

    if(move_uploaded_file($pic['tmp_name'], $upload_dir.$new_name)){
        //Dodaj rekord do bazy
        mysql_query(
            "INSERT INTO images (imgURL, placeID)
            VALUES ('$new_name', '$place_id')")
            or exit_status('Błąd przesyłania!');

        exit_status('Plik przesłany poprawnie!');
    }

}

exit_status('Błąd przesyłania!');

function exit_status($str){
    echo json_encode(array('status'=>$str));
    exit;
}

function get_extension($file_name){
    $ext = explode('.', $file_name);
    $ext = array_pop($ext);
    return strtolower($ext);
}