/**
 * Created by Ada on 2015-03-07.
 */
$( document ).ready(function() {
$('#my_map_canvas').gmap({ 'zoom':6,'center': new google.maps.LatLng(52.348763181988076, 19.51171875), 'callback': function() {
    $.getJSON( 'my_points.php', function(data) {
        $.each( data.markers, function(i, m) {
            var _coord = m[7];
            var _coordmap = _coord.replace("(", "");
            _coordmap = _coordmap.replace(")", "");
            var link =  '<button class="btn btn-link" onclick='+'"showComments('+ m[0]+')"'+ '>' +  m[2] + '</button>'
            $('#my_map_canvas').gmap('addMarker', { 'position': _coordmap } ).click(function() {
                $('#my_map_canvas').gmap('openInfoWindow', {'content': link}, this);
            });
        });
    });
}}).bind('init', function(event, map) {
    $(map).click( function(event) {
        $('#my_map_canvas').gmap('addMarker', {
            'position': event.latLng,
            'draggable': false,
            'bounds': false
        }, function(map, marker) {
            $('#dialog').append('<form id="dialog'+marker.__gm_id+'" method="get" action="/" style="display:none;">' +
            '<p><label for="name">Nazwa</label><input id="name'+marker.__gm_id+'" class="txt" name="name" value="" required="required"/></p>' +
            '<p><label for="coordinates">Współrzędne geogr.</label><input id="coordinates'+marker.__gm_id+'" class="txt" name="coordinates" value=""/></p>' +
            '<p><label for="state">Wojwództwo</label><input id="state'+marker.__gm_id+'" class="txt" name="state" value=""/></p>'+
            '<p><label for="country">Państwo</label><input id="country'+marker.__gm_id+'" class="txt" name="country" value=""/></p>' +
            '<p><label for="description">Opis</label><textarea id="description" class="txt" name="description" cols="40" rows="5"></textarea></p>' +
            '<p><label for="tips">Wskazówki</label><textarea id="tips" class="txt" name="tips" cols="40" rows="5"></textarea></p>' +
            '<p><label> Zdjęcia </label></p>'+
            '<p><label for="image-no" class="radio-inline"><input type="radio" name="image" id="image-no" value="no" checked> NIE </label>' +
            '<p><label for="image-yes" class="radio-inline"><input type="radio" name="image" id="image-yes" value="yes"> TAK </label>' +
            '</form>');
            findLocation(marker.getPosition(), marker);
        }).click( function() {
            openDialog(this);
        })
    });
});

function findLocation(location, marker) {
    $('#my_map_canvas').gmap('search', {'location': location}, function(results, status) {
        if ( status === 'OK' ) {
            $.each(results[0].address_components, function(i,v) {
                if ( v.types[0] == "administrative_area_level_1" ||
                    v.types[0] == "administrative_area_level_2" ) {
                    $('#state'+marker.__gm_id).val(v.long_name);
                } else if ( v.types[0] == "country") {
                    $('#country'+marker.__gm_id).val(v.long_name);
                }
            });
            $('#coordinates'+marker.__gm_id).val(location);
            openDialog(marker);
        }
    });
}

function openDialog(marker) {
    $('#dialog'+marker.__gm_id).dialog({'width':400,'modal':true, 'title': 'Edytuj i zapisz punkt', 'buttons': {
        "Cofnij": function() {
            $(this).dialog( "close" );
            marker.setMap(null);
            marker=null;
            $('#dialogundefined').remove();
        },
        "Zapisz": function() {
            $.ajax({
                url: 'send_place.php',
                type: 'POST',
                data: $('#dialogundefined').serialize(),
                success:
                    function(result) {

                        //jeśli zaznaczono 'Dodaj zdjęcia':
                        var addimg = ($("input[name=image]" ).serialize());
                        //alert(addimg);
                        $('#dialogundefined').remove();
                        if (addimg == "image=yes" ) {
                            alert('Przeciągnij pliki z obrazami do czarnego prostokąta na dole strony.')
                            $('#dropbox').removeClass('invisible');
                            $('#closebtn').removeClass('invisible');
                            rundropbox(result);
                            }
                    }
            });
            $(this).dialog( "close" );
        }
    }});
}

$(function() {
    var listplace = $("#listplace");
    listplace.append("<option disabled>...</option>");
    $.getJSON( 'my_points.php', function(data) {
        $.each(data.markers, function (i, m) {
            listplace.append("<option value='"+m[7]+';'+m[0] +"'>"+m[2] +'</option>')
            listplace.selectmenu({
                change: function( event, data ) {
                    var tmp = data.item.value.split(";");
                    var coords = tmp[0].slice(1,-1);
                    var tmp2 = coords.split(",");
                    var newLatLng = new google.maps.LatLng(parseFloat(tmp2[0]), parseFloat(tmp2[1]));
                    $('#my_map_canvas').gmap('get','map').setOptions({'center':newLatLng,'zoom':9});
                    showComments(tmp[1]);
                }
            });
        });
    })
});
    $(function() {
        var listcustom = $("#listcustom");
        listcustom.append("<option disabled>...</option>");
        $.getJSON( 'custom_points.php', function(data) {
            $.each(data.markers, function (i, m) {
                listcustom.append("<option value='"+m[7]+';'+m[0] +"'>"+m[2] +'</option>')
                listcustom.selectmenu({
                    change: function( event, data ) {
                        var tmp = data.item.value.split(";");
                        var coords = tmp[0].slice(1,-1);
                        var tmp2 = coords.split(",");
                        var newLatLng = new google.maps.LatLng(parseFloat(tmp2[0]), parseFloat(tmp2[1]));
                        $('#my_map_canvas').gmap('get','map').setOptions({'center':newLatLng,'zoom':9});
                        showComments(tmp[1]);
                    }
                });
            });
        })
    })

});


function showComments(placeID) {
    $(".comment_section_profile").html('').append('<h3>Komentarze:</h3>');

    $.getJSON( 'place.php?id='+placeID, function(result) {

        console.log(result);
        $("#ajax_content_profile").html('');
        $("#ajax_content_profile")
            .append(
            '<h3> Opis </h3>'+
            '<div class="panel panel-default">'+
            '<div class="panel-heading">'+ result['name'] +

            '</div>'+
            '<div class="panel-body">'+
            '<p>'+result['country']+'</p>'+
            '<p>'+'Współrzędne: '+ result['coordinates']+'</p>'+
            '<p>'+'Dodał(a): '+ result['login']+'</p>'
        );
        if (result['is_owner']=='yes') {
            $("#ajax_content_profile").append('<button class="btnDelete btn btn-danger" onclick="deletePlace(' + placeID + ');">Usuń punkt</button>');
        }
    });

    $.getJSON( 'get_comments.php?id='+placeID, function(data) {
        $.each( data.comments, function(i, m) {
            var data = new Date(m[2]*1000);
            data = data.getDate()+'-'+data.getMonth()+'-'+data.getFullYear();
            $(".comment_section_profile").append(
            '<div class="panel panel-default">' +
                '<div class="panel-heading">' +
                    '<h3 class="panel-title">'+m[1]+', '+data+'</h3>'+
                '</div>'+
                '<div class="panel-body">'+m[0]+
                '</div>'+
            '</div>');
        });
    });

    $.getJSON( 'get_images.php?id='+placeID, function(result) {
        $('.carousel-indicators').html('');
        $('.carousel-inner').html('');
        if (!(result.images=='no')) {
            $("#place-carousel").removeClass('invisible').addClass('slide');
            $.each(result.images, function (i, m) {
                if (i == 0) {
                    $('.carousel-indicators').append('<li data-target="#place-carousel" data-slide-to="0" class="active"></li>');
                    $('.carousel-inner').append(
                        '<div class="item active">' +
                        '<img src="uploads/' + m + '" alt="...">' +
                        '</div>')
                } else {
                    $('.carousel-indicators').append('<li data-target="#place-carousel" data-slide-to="' + i + '"></li>');
                    $('.carousel-inner').append(
                        '<div class="item">' +
                        '<img src="uploads/' + m + '" alt="...">' +
                        '</div>')
                }
            });
        } else {
            $("#place-carousel").removeClass('slide').addClass('invisible');
        }
    });
};


// przesyłanie obrazu - drag and drop
function rundropbox(placeid) {
    var dropbox = $('#dropbox'),
        message = $('.message', dropbox);

    dropbox.filedrop({
        // Tymczasowa nazwa dla $_FILES:
        paramname:'pic',
        maxfiles: 1,
        maxfilesize: 1, // 1 mb
        withCredentials: false,
        url: 'post_file.php?id=' + placeid,

        uploadFinished: function(i,file,response){
            $.data(file).addClass('done');
        },

        error: function(err, file) {
            switch(err) {
                case 'BrowserNotSupported':
                    showMessage('Twoja przeglądarka nie wspiera przesyłanie plików w HTML5!');
                    break;
                case 'TooManyFiles':
                    alert('Proszę przeciągać pliki pojedynczo.');
                    break;
                case 'FileTooLarge':
                    alert(file.name+' jest za duży! Limit: 1Mb.');
                    break;
                default:
                    break;
            }
        },
        beforeEach: function(file){
            if(!file.type.match(/^image\//)){
                alert('Tylko pliki graficzne są dozwolone!');
                return false;
            }
        },

        uploadStarted:function(i, file, len){
            createImage(file);
        },

        progressUpdated: function(i, file, progress) {
            $.data(file).find('.progress').width(progress);
        }

    });
    var template = '<div class="preview">'+
                        '<span class="imageHolder">'+
                            '<img />'+
                            '<span class="uploaded"></span>'+
                        '</span>'+
                        '<div class="progressHolder">'+
                            '<div class="progress"></div>'+
                        '</div>'+
                    '</div>';

    function createImage(file){

        var preview = $(template),
            image = $('img', preview);

        var reader = new FileReader();

        image.width = 100;
        image.height = 100;

        reader.onload = function(e){
            image.attr('src',e.target.result);
        };
        reader.readAsDataURL(file);
        preview.appendTo(dropbox);
        $.data(file,preview);
    }


    function showMessage(msg){
        message.html(msg);
    }

};

function finnishUpload () {
    location.reload();
}
function deletePlace(pID) {
    $.ajax({
        url: 'delete_place.php?placeid='+pID,
        dataType: 'text',
        type: 'post',
        success: function (output) {
            $('.btnDelete').text('Usunięto');
            window.setTimeout( location.reload(), 1500 )

        }
    });
}

$( document ).ready(function() {
    $('#my_map_canvas').tooltip({
        }
    );
});