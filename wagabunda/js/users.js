/**
 * Created by Ada on 2015-03-21.
 */
$(document).ready(function($) {
    $.ajax({
            url: 'zalogowany.php',
            dataType: 'json',
            type: 'get',
            success: function (output) {
                if (output == 'notlogged') {
                    $('#navigation').append(
                        '<li><a class="btn btn-default" href="login.php">Zaloguj</a></li> ' +
                        '<li><a class="btn btn-default" href="register.php">Zarejestruj</a></li> '
                    );
                } else {
                    $('#navigation').append(
                        '<li><a class="" href="profile.html?id=' + output['userID'] + '"' + '>Twój profil</a></li>'                    );
                    if (output['is_moderator']==1) {
                        $('#navigation').append(('<li class="active_site"><a class="" href="users.html">Wyświetl użytkowników</a></li>'));
                    }
                    $('#navigation').append(
                        '<li><a class="" href="logout.php">Wyloguj się</a></li>'
                    );

                }
            }
        }
    );

    $.getJSON('users.php', function (data) {
            $.each( data.users, function(i, m) {
                var data = new Date(m[5]*1000);
                data = data.getDate()+'-'+data.getMonth()+'-'+data.getFullYear();

                if (m[4]) {
                    var delbtn = '<button class="btnDeleteUser" onclick="deleteUser(' + m[0] + ');">X</button>';
                }

                $("#users-table").append(
                    '<tr data-id='+m[0]+'>'+
                    '<th>' + i +'</th>' +
                    '<th>' + m[1]+ '</th>' +
                    '<th>' + m[2]+ '</th>' +
                    '<th>' + m[4]+ '</th>' +
                    '<th>' + data + '</th>' +
                    '<th>' + delbtn + '</th>' +
                    '</tr>'
                );
            });
        }
    );
});

function deleteUser(uID) {
    var selector = 'tr[data-id='+ uID + ']';
    $.ajax({
        url: 'delete_user.php?userid=' + uID,
        dataType: 'text',
        type: 'post',
        success: function (output) {
            $(selector).hide('slow');
        }
    });
}