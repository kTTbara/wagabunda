/**
 * Created by Ada on 2015-03-09.
 */

$(document).ready(function($) {
    $.ajax({
            url: 'zalogowany.php',
            dataType: 'json',
            type: 'get',
            success: function (output) {
                if (output == 'notlogged') {
                    $('#navigation').append(
                        '<li><a href="" class="" data-toggle="modal" data-target="#loginModal">'+
                        'Zaloguj' +
                        '</button></li>' +
                        '<li><a class="" href="" data-toggle="modal" data-target="#registerModal">'+
                        'Zarejestruj' +
                        '</button></li>'
                    );
                } else {
                    $('#navigation').append(
                        '<li><a class="" href="profile.html?id=' + output['userID'] + '"' + '>Twój profil</a></li>'
                    );
                    if (output['is_moderator']==1) {
                        $('#navigation').append('<li><a class="" href="users.html">Wyświetl użytkowników</a></li>');
                    };
                    $('#navigation').append(
                    '<li><a class="t" href="logout.php">Wyloguj się</a></li>'
                    );

                }
            }
        }
    );

    $('#all_map_canvas').gmap({ 'zoom':6, 'center':new google.maps.LatLng(52.348763181988076, 19.51171875), 'callback': function() {
        $('#listplace_all').append("<option disabled>...</option>");
        $.getJSON( 'all_points.php', function(data) {
            $.each( data.markers, function(i, m) {
                var _coord = m[7];
                var _coordmap = _coord.replace("(", "");
                _coordmap = _coordmap.replace(")", "");

                $('#all_map_canvas').gmap('addMarker', { 'position': _coordmap } ).click(function() {
                    var link =  '<button class="btn btn-link" onclick='+'"showComments('+ m[0]+')"'+ '>' +  m[2] + '</button>'

                    $('#all_map_canvas').gmap('openInfoWindow', {
                        'content': link
                    }, this);

                });
                $('#listplace_all').append("<option value='"+m[7]+';'+m[0] +"'>"+m[2] +'</option>');
                $( "#listplace_all" ).selectmenu({
                    change: function( event, data ) {
                        var tmp = data.item.value.split(";");
                        var coords = tmp[0].slice(1,-1);
                        var tmp2 = coords.split(",");
                        var newLatLng = new google.maps.LatLng(parseFloat(tmp2[0]), parseFloat(tmp2[1]));
                        $('#all_map_canvas').gmap('get','map').setOptions({'center':newLatLng,'zoom':9});
                        showComments(tmp[1]);
                    }
                });

            });

        });
    }});
});

function showPlace (_coordmap) {
    _coordmap = _coordmap.replace("(", "");
    _coordmap = _coordmap.replace(")", "");

    $('#place_map_canvas').gmap().bind('init', function(ev, map) {
        $('#place_map_canvas').gmap('addMarker', {'position': _coordmap, 'bounds': true});
    });
};

function showComments(placeID) {
    $('#input_comment').attr('data-id', String(placeID));
    $(".comment-section").html('');

    $.getJSON( 'place.php?id='+placeID, function(result) {
        $("#ajax-content").html('');
        $("#ajax-content").append('<div class="panel panel-default">'+
            '<div class="panel-heading">'+ result['name'] +

            '</div>'+
            '<div class="panel-body">'+
            '<p>'+result['country']+'</p>'+
            '<p>'+'Współrzędne: '+ result['coordinates']+'</p>'+
            '<p>'+'Dodał(a): '+ result['login']+'</p>' +
            '<p>'+'Opis: '+ result['description']+'</p>'+
            '<p>'+'Wskazówki: '+ result['tips']+'</p>'

        );
        if (result['is_moderator']!=null) {
            $("#ajax-content").append('<button class="btnToList btn btn-success" onclick="addToList('+placeID+');">Dodaj do schowka</button>');}
        if (result['is_moderator']==1 || result['is_owner']=='yes') {
            $("#ajax-content").append('<button class="btnDelete btn btn-danger" onclick="deletePlace(' + placeID + ');">Usuń punkt</button>');
        }
    });


    $.getJSON( 'get_images.php?id='+placeID, function(result) {
        $('.carousel-indicators').html('');
        $('.carousel-inner').html('');
        if (!(result.images=='no')) {
            $("#place-carousel").removeClass('invisible').addClass('slide');
            $.each(result.images, function (i, m) {
                if (i == 0) {
                    $('.carousel-indicators').append('<li data-target="#place-carousel" data-slide-to="0" class="active"></li>');
                    $('.carousel-inner').append(
                        '<div class="item active">' +
                        '<img src="uploads/' + m + '" alt="...">' +
                        '</div>')
                } else {
                    $('.carousel-indicators').append('<li data-target="#place-carousel" data-slide-to="' + i + '"></li>');
                    $('.carousel-inner').append(
                        '<div class="item">' +
                        '<img src="uploads/' + m + '" alt="...">' +
                        '</div>')
                }
            });
        } else {
            $("#place-carousel").removeClass('slide').addClass('invisible');
        }
    });

    $.ajax({
        url: 'zalogowany.php',
        dataType: 'json',
        type: 'get',
        success: function (output) {

            $('.comments').removeClass('invisible');
            if (output != 'notlogged') {
                $('#add-comment').removeClass('invisible');
            } else {
                $('.btnToList').remove();

            }
        }
    });


    $.getJSON( 'get_comments.php?id='+placeID, function(data) {
        $.each( data.comments, function(i, m) {
            var data = new Date(m[2]*1000);
            data = data.getDate()+'-'+data.getMonth()+'-'+data.getFullYear();
            $(".comment-section").append(
                '<div class="panel panel-default">' +
                '<div class="panel-heading">' +
                '<h3 class="panel-title">'+m[1]+', '+data+'</h3>'+
                '</div>'+
                '<div class="panel-body">'+m[0]+
                '</div>'+
                '</div>');
        });
    });
};


$(document).ready(function($){
    $('#add-comment').submit(function(event){
            event.preventDefault();
            $.ajax({
                'data' : {
                    'comment' : $("#input_comment").val(),
                    'placeID' : $("#input_comment").attr('data-id')
                },
                'url' : './add_comment.php',
                'dataType' : 'json',
                'type' : 'post',
                'success' : function(response) {
                if (response.type == 'success') {
                    $(".comment-section").append(
                        '<div class="panel panel-default">' +
                        '<div class="panel-heading">' +
                        '<h3 class="panel-title"> ja, teraz </h3>'+
                        '</div>'+
                        '<div class="panel-body">'+$("#input_comment").val()+
                        '</div>'+
                        '</div>');

                    $("#input_comment").empty();
                } else {
                    alert('nie ok');
                }
            }
            });
            }

            );

    $( "form" ).on( "submit", function( event ) {
        var userdata = $('#loginForm').serialize();
        $.ajax({
            url: 'login.php',
            data: userdata,
            dataType: 'json',
            type: 'post',
            success: function (output) {
                //$('.btnToList').text('Dodano');
            }
        });
    });
});
function addToList(pID) {
    $.ajax({
        url: 'add_to_list.php?placeid=' + pID,
        dataType: 'text',
        type: 'post',
        success: function (output) {
            $('.btnToList').text('Dodano');
        }
    });
}
function deletePlace(pID) {
    $.ajax({
        url: 'delete_place.php?placeid='+pID,
        dataType: 'text',
        type: 'post',
        success: function (output) {
            $('.btnDelete').text('Usunięto');
            window.setTimeout( window.location.reload, 1500 )
        }
    });
}

