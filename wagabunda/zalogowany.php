<?php
/**
 * Created by PhpStorm.
 * User: Ada
 * Date: 2015-03-16
 * Time: 16:27
 */
include 'config.php';
connect_to_db();
// sprawdzamy czy użytkownik jest zalogowany
if ($_SESSION['logged']) {
    $user_data = get_user_data();
    die (json_encode($user_data));
} else {
    die(json_encode('notlogged'));
}
close_conn();