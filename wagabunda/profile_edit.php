<?php
/**
 * Created by PhpStorm.
 * User: Ada
 * Date: 2015-03-04
 * Time: 21:45
 */
include 'config.php';
connect_to_db();
 
check_login();
 
$user_data = get_user_data();
 
// po kliknięciu przycisku "Edytuj profil"
if(isset($_POST['email'])) {
    $_POST['new_password'] = clear($_POST['new_password']);
    $_POST['new_password_rep'] = clear($_POST['new_password_rep']);
    $_POST['password'] = clear($_POST['password']);
    $_POST['email'] = clear($_POST['email']);

    // zmienne tymczasowe na treść błędu
    $err = '';
    // i zapytanie sql
    $sql = '';

    // po podaniu nowego hasła lub nowego emaila
    if(!empty($_POST['new_password']) || $_POST['email'] != $user_data['email']) {
        // sprawdzamy czy zostało podane aktualne hasło
        if(empty($_POST['password'])) {
            $err = '<p>Należy podać stare hasło.</p>';
            // sprawdzenie czy hasło jest poprawne
        } elseif(encode($_POST['password']) != $user_data['password']) {
            $err = '<p>Podane hasło jest nieprawidłowe.</p>';
        } else {
            // jeśli hasło jest prawidłowe
            if(!empty($_POST['new_password'])) {
                // jeśli podane dwa hasła są różne:
                if($_POST['new_password'] != $_POST['new_password_rep']) {
                    $err = '<p>Podane hasła nie są takie same.</p>';
                    // tworzenie zapytania do aktualizacji hasła
                } else {
                    $sql.= ", `password` = '".encode($_POST['new_password'])."'";
                }
            }
            // sprawdzamy czy użytkownik chce zmienić email (czy ten podany jest różny od aktualnego)
            if($_POST['email'] != $user_data['email']) {
                // sprawdzamy czy podany email jest poprawny
                if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
                    $err = '<p>Podany email nie jest prawidłowy.</p>';
                } else {
                    // sprawdzenie czy istnieje już taki adres email należący do innego użytkownika
                    $result = mysql_query("SELECT Count(userID) FROM `users` WHERE `userID` != '{$user_data['userID']}' AND `email` = '{$_POST['email']}'");
                    $row = mysql_fetch_row($result);
                    if($row[0] > 0) {
                        $err = '<p>Login lub adres e-mail są już zajęte.</p>';
                    } else {
                        // zapytanie do aktualizacji emaila
                        $sql.= ", `email` = '{$_POST['email']}'";
                    }
                }
            }
        }
    }

    // wyświetlanie błędów
    if(!empty($err)) {
        echo $err;
    } else {
        // jeśli nie ma błędów - wykonanie aktualizacji danych logowania
        $result = mysql_query("UPDATE `users` SET {$sql} WHERE `userID` = '{$user_data['userID']}'");
        if($result) {
            echo '<p>Aktualizacja profilu przebiegła pomyślnie.</p>';
            $user_data = get_user_data();
        } else {
            echo '<p>Wystąpił błąd:<br>'.mysql_error().'</p>';
        }
    }
}
 
// formularz
echo '<form method="post" action="profile_edit.php">
    <p>
        Login:<br>
        <input type="text" value="'.$user_data['login'].'" disabled>
    </p>
    <p>
        Nowe hasło (pozostaw to pole puste jeśli nie chcesz zmieniać):<br>
        <input type="password" value="" name="new_password" autocomplete="off">
    </p>
    <p>
        Powtórz nowe hasło:<br>
        <input type="password" value="" name="new_password_rep" autocomplete="off">
    </p>
    <p>
        E-mail:<br>
        <input type="text" value="'.$user_data['email'].'" name="email">
    </p>
    <p>
        Aktualne hasło (wymagane przy zmianie emaila lub hasła):<br>
        <input type="password" value="" name="password" autocomplete="off">
    </p>
    <p>
        <input type="submit" value="Edytuj profil">
    </p>
</form>';

close_conn();